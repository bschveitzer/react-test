# React test

I've used the tests requirements as a guide and tried to stay in the 2 hours time limit, it passed a little bit. Within this time was possible to create the core feature but I couldn't write tests and improve the style.  
I've used some eslint templates and scripts as reference to keep to code clean. With the AntD library, it was used the styled components which in my opinion improves the cleanse of the code and its easy to maintain. For the core, it was used graphql/apollo separated in folders by type of queries, it keeps the same type queries unified. The base scructure was created with create-react-app.  
Since it didn't need to change pages, I didn't used the react-router and no need for state management so I didn't use redux.  
I'm open to any doubts and hope to talk to you guys soon!

## Available Scripts

### `yarn`

Install all dependencies.

### `yarn lint`
Verify and fix all lint errors.

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).