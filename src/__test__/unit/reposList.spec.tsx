import React from 'react';
import { ReposList } from '../../components/ReposList/index';
import { render, RenderResult } from "@testing-library/react";

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // deprecated
    removeListener: jest.fn(), // deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  })),
});

let documentBody: RenderResult;

describe('<ReposList />', () => {
  beforeEach(() => {
    documentBody = render(<ReposList />);
  });

  it('should render without results', () => {
    expect(documentBody.getByText('Name')).toBeInTheDocument();
    expect(documentBody.getByText('Stars')).toBeInTheDocument();
    expect(documentBody.getByText('Forks')).toBeInTheDocument();
    expect(documentBody.getByText('No Data')).toBeInTheDocument();
  });
});