import { gql } from '@apollo/client';

// Graphql query for fetching github repositories.
// Params: search_term it will fetch based on this params. 
// TODO: accept pagination as param, it will be set as the 'first'.
export const SEARCH_REPOS = gql`
    query($search_term: String!) {
        search(query: $search_term, type: REPOSITORY, first: 10) {
            repositoryCount,
            edges {
                node {
                    ... on Repository {
                        name,
                        forkCount,
                        url,
                        stargazerCount
                    }
                }
            }
        }
    }
`;