import React, { FunctionComponent } from 'react';
import { Table } from 'antd';
import { RepoLink } from './style';

export const ReposList: FunctionComponent<ReposListProps> = ({loading, results}) => {
  // Determines the table fields
  // Source doc: https://ant.design/components/table/
  const columns = [
    {
      title: 'Name',
      dataIndex: ['node', 'name'],
      key: 'node.name',
      render: (text: String, record: IRepoItem) => <RepoLink href={record.url} target="_blank">{text.toUpperCase()}</RepoLink>,
    },
    {
      title: 'Stars',
      dataIndex: ['node', 'stargazerCount'],
      key: 'node.stargazerCount',
    },
    {
      title: 'Forks',
      dataIndex: ['node', 'forkCount'],
      key: 'node.forkCount',
    },
  ];

  // Check if the fetching return came with data and renders the table.
  return (
    <Table dataSource={results?.search?.edges} columns={columns} loading={loading}/> 
  );
}

interface ReposListProps {
  loading?: boolean,
  results?: { search: { edges: IRepoItem[]}} | undefined
}

interface IRepoItem {
  url: string,
  name: string,
  stargazerCount: Number,
  forkCount: Number
}
