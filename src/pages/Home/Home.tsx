import React, { FunctionComponent } from 'react';
import { Wrapper } from './style';
import { ReposList } from '../../components/ReposList';
import { SearchInput } from '../../components/SearchInput';
import { useLazyQuery } from "@apollo/react-hooks";
import { SEARCH_REPOS } from '../../api/graphql/queries/Repos';

export const Home: FunctionComponent = () => {
  // Fetch all repos in github with the search therm.
  const [handleSearch, { loading, data }] = useLazyQuery(SEARCH_REPOS);

  return (
    <Wrapper>
      <SearchInput 
        request={(searchTerm: string) => handleSearch({variables: {search_term: searchTerm}})} 
        placeholder='Search GitHub repositories...'/>
      <ReposList loading={loading} results={data}/>
    </Wrapper>
  );
}